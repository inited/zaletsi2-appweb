<?php

$requestURI = $_SERVER['REQUEST_URI'];
if ($requestURI == '/.well-known/apple-app-site-association') {
  header("Content-type: application/json");
  $content = file_get_contents('.well-known/apple-app-site-association');
  echo $content;
  exit;
}


// stahni puvodni stranku
$zaletsiPage = file_get_contents("https://zaletsi.cz" . $_SERVER['REQUEST_URI']);

$pastedText = "";
$separator = "\n";
$line = strtok($zaletsiPage, $separator);


// vytahej z ni meta data a titulek - aby se dobre zobrazovala v nahledech
while ($line !== false) {

    if (
        (strpos($line, "<meta property=") !== false)
        || (strpos($line, "<title>") !== false)
    ) {
        $line = preg_replace('/^[^<]+/', '', $line);
        $pastedText .= $line . "\n";
        if (strpos($line, "<title>") !== false) {
          $title = preg_replace('/<title>/', "", preg_replace('/ - Zaletsi.cz<\/title>/', "", $line));
        }
    }

    # do something with $line
    $line = strtok( $separator );
}


?>

<html>
<head>
<?php echo $pastedText ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, user-scalable=no">
<link rel="stylesheet" type="text/css" href="/styles.css">
</head>
<body>
  <header>
    <div class="container">
      <a href="/">
  			<img src="/images/logo.svg" width="230" height="68" alt="Logo ZaleťSi.cz">
  		</a>
    </div>
  </header>
  <div class="container">
    <div class="box">
      <h1>Stáhněte si aplikaci</h1>
      <p>Nejlevnější letenky se rychle objeví a stejně tak rychle zmizí. Nastav si mobilní notifikace, nech si posílat nejlepší pecky do mailu či je sleduj v prohlížeči. Tady je několik tipů, jak sledovat letenky tak, aby ti už žádná neunikla!</p>
      <div class="download">
        <a href="https://play.google.com/store/apps/details?id=cz.zaletsi.hlidac" target="_blank" class="btn">
          <span class="label">Stáhnout pro Android</span>
          <span class="icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="15px" height="15px"><path d="M325.3 234.3L104.6 13l280.8 161.2-60.1 60.1zM47 0C34 6.8 25.3 19.2 25.3 35.3v441.3c0 16.1 8.7 28.5 21.7 35.3l256.6-256L47 0zm425.2 225.6l-58.9-34.1-65.7 64.5 65.7 64.5 60.1-34.1c18-14.3 18-46.5-1.2-60.8zM104.6 499l280.8-161.2-60.1-60.1L104.6 499z"></path></svg>
          </span>
        </a>
        <a href="https://apps.apple.com/ph/app/zaletsi-cz-hlidac-letenek/id1117981323" target="_blank" class="btn">
          <span class="btn__label">Stáhnout pro iOS</span>
          <span class="btn__icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 376.5 512" width="15px" height="15px"><path d="M314.7 268.7c-.2-36.7 16.4-64.4 50-84.8-18.8-26.9-47.2-41.7-84.7-44.6-35.5-2.8-74.3 20.7-88.5 20.7-15 0-49.4-19.7-76.4-19.7C59.3 141.2 0 184.8 0 273.5c0 26.2 4.8 53.3 14.4 81.2 12.8 36.7 59 126.7 107.2 125.2 25.2-.6 43-17.9 75.8-17.9 31.8 0 48.3 17.9 76.4 17.9 48.6-.7 90.4-82.5 102.6-119.3-65.2-30.7-61.7-90-61.7-91.9zm-56.6-164.2c27.3-32.4 24.8-61.9 24-72.5-24.1 1.4-52 16.4-67.9 34.9-17.5 19.8-27.8 44.3-25.6 71.9 26.1 2 49.9-11.4 69.5-34.3z"></path></svg>
          </span>
        </a>
        <p>
          <a href="http://zaletsi.cz<?php echo $_SERVER['REQUEST_URI'] ?>">Zobrazit akci <b><?php echo $title; ?></b> na webu</a>
        </p>
      </div>
    </div>
  </div>
</body>
</html>
